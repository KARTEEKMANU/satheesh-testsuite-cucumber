$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("./src/test/java/com/newtopia/features/myday.feature");
formatter.feature({
  "name": "Test the My Day page",
  "description": "",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "name": "test the My Day page",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "I have to login to sf",
  "keyword": "Given "
});
formatter.step({
  "name": "I am on the home page of sf and click on events button",
  "keyword": "When "
});
formatter.step({
  "name": "i navigate to the events page and create new events with status \u003cstatus\u003e type \u003ctype\u003e source \u003csource\u003e method \u003cmethod\u003e",
  "keyword": "And "
});
formatter.step({
  "name": "i navigate to the edit event details page and capture all the details",
  "keyword": "And "
});
formatter.step({
  "name": "i navigate back to coaching portal",
  "keyword": "And "
});
formatter.step({
  "name": "i validate my day portal",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "status",
        "type",
        "source",
        "method"
      ]
    },
    {
      "cells": [
        "scheduled",
        "coaching_session",
        "Inbound",
        "phone"
      ]
    }
  ]
});
formatter.background({
  "name": "instantiate the chrome driver",
  "description": "",
  "keyword": "Background"
});
formatter.step({
  "name": "i instantiate the chrome driver",
  "keyword": "Given "
});
formatter.match({
  "location": "DriverConfig.java:15"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "test the My Day page",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "I have to login to sf",
  "keyword": "Given "
});
formatter.match({
  "location": "MyDaySteps.java:34"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I am on the home page of sf and click on events button",
  "keyword": "When "
});
formatter.match({
  "location": "MyDaySteps.java:50"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "i navigate to the events page and create new events with status scheduled type coaching_session source Inbound method phone",
  "keyword": "And "
});
formatter.match({
  "location": "MyDaySteps.java:64"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "i navigate to the edit event details page and capture all the details",
  "keyword": "And "
});
formatter.match({
  "location": "MyDaySteps.java:97"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "i navigate back to coaching portal",
  "keyword": "And "
});
formatter.match({
  "location": "MyDaySteps.java:120"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "i validate my day portal",
  "keyword": "Then "
});
formatter.match({
  "location": "MyDaySteps.java:133"
});
formatter.result({
  "status": "passed"
});
});