How to execute the project

`mvn clean test`

Where are the step definitions present 

`src >test >java> com.newtopia > stepDefinitions`

Where are the features present

`src >test > features`

Where is the test runner class present

`src >test >java> com.newtopia >` -> `CucumberRunnerTest.java`

