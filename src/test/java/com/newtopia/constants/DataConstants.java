package com.newtopia.constants;

public class DataConstants {

    public static final String SCHEDULED = "scheduled";
    public static final String MISSED = "missed";
    public static final String CANCELLED = "cancelled";
    public static final String COMPLETED = "completed";
    public static final String UNAVAILABLE = "unavailable";
    public static final String RESCHEDULED = "Rescheduled";

    public static final String COACHING_SESSION = "coaching_session";
    public static final String INBOUND = "Inbound";
    public static final String PHONE = "phone";

}
