package com.newtopia.stepDefinitions;

import com.newtopia.constants.DataConstants;
import com.newtopia.constants.UrlConstants;
import com.newtopia.data.EventDetails;
import com.newtopia.pages.*;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import java.awt.*;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import static com.newtopia.constants.UrlConstants.*;

public class DriverSetup {

    public static WebDriver driver;
    private SFHomePage sfHomePage;
    private EventDetails eventDetails;
    private String startTime;
    private static String notes = "Test notes " + Calendar.getInstance().getTime();

    @BeforeSuite
    public void setUpDriver() {
        String osName = System.getProperty("os.name");
        if (osName.contains("MAC") || osName.contains("Mac")) {
            System.setProperty("webdriver.chrome.driver", "src/test/java/com/newtopia/divers/chromedriver_mac");
            driver = new ChromeDriver();
        } else {
            System.out.println("The os is not determined");
        }
    }

    @Test(priority = 1, description = "login to the sales force website")
    public void loginToSalesForce() {
        driver.get(SALESFORCE_URL);
        driver.manage().window().maximize();
        Assert.assertEquals(driver.getTitle(), "Login | Salesforce", "Login page title validation failed");
        LoginPage loginPage = new LoginPage(driver);
        loginPage.loginPageValidator(emailId, UrlConstants.password);

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        //waiting for the page to load
        sfHomePage = new SFHomePage(driver);
        WebDriverWait webDriverWait = new WebDriverWait(driver, 120);
        webDriverWait.until(ExpectedConditions.visibilityOf(sfHomePage.getAllTabsButton()));
        Assert.assertEquals(driver.getTitle(), "Salesforce - Enterprise Edition", "Title after login validation failed");
    }

    @Test(priority = 2, dependsOnMethods = "loginToSalesForce", description = "open events")
    public void navigateToEvents() {
        sfHomePage.getAllTabsButton().click();
        System.out.println("+ Button is clicked");

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        //scroll to the coaching portal object
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].scrollIntoView(true);", sfHomePage.getEventsButton());

        sfHomePage.getEventsButton().click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @Test(priority = 3, dependsOnMethods = "navigateToEvents", description = "create new event")
    public void createNewEvent() throws InterruptedException {
        EventsPage eventsPage = new EventsPage(driver);
        eventsPage.getNewButton().click();

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        CreateEventsPage createEventsPage = new CreateEventsPage(driver);
        createEventsPage.getParticipantTextBx().click();
        Thread.sleep(3000);
        createEventsPage.getParticipantTextBx().sendKeys(PARTICIPANT);

        Select select = new Select(createEventsPage.getStatusTab());
        select.selectByVisibleText(DataConstants.SCHEDULED);

        Select typeSelect = new Select(createEventsPage.getTypeTab());
        typeSelect.selectByVisibleText(DataConstants.COACHING_SESSION);

        Select sourceSelect = new Select(createEventsPage.getSourceTab());
        sourceSelect.selectByVisibleText(DataConstants.INBOUND);

        createEventsPage.getDatePicker().click();

        createEventsPage.getNotesTextField().click();
        createEventsPage.getNotesTextField().sendKeys(notes);

        Select methodSelect = new Select(createEventsPage.getMethod());
        methodSelect.selectByVisibleText(DataConstants.PHONE);

        createEventsPage.getSaveBtn().click();
    }


    @Test(priority = 4, dependsOnMethods = "createNewEvent", description = "Navigate to the conformation page")
    public void getDetailsOfEditPage() throws InterruptedException {
        Thread.sleep(5000);
        EventEditDetailsPage eventEditDetailsPage = new EventEditDetailsPage(driver);
        System.out.println("the start time is " + eventEditDetailsPage.getStartTime());

        String startDate = eventEditDetailsPage.getStartTime();
        String time = startDate.substring(10);
        System.out.println("time " + time + " length is " + time.length());
        if(time.length() == 7) {
            startTime = "0" + startDate.substring(10); //appending '0' example '7:14 PM' becomes '07:14 PM'
        }
        else {
            startTime = startDate.substring(10);
        }
        System.out.println("time is [" + startTime + "]");

        eventDetails = new EventDetails();
        eventDetails.setEventStartTime(startTime);
        eventDetails.setEventNotes(notes);

        //TODO get all the event details
    }

    @Test(priority = 5, dependsOnMethods = "getDetailsOfEditPage", description = "Navigate Back to Coaching Portal")
    public void navigateToCoachingPortal() {
        sfHomePage.getAllTabsButton().click();
        System.out.println("+ Button is clicked");

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        //scroll to the coaching portal object
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].scrollIntoView(true);", sfHomePage.getCoachingPortalButton());

        sfHomePage.getCoachingPortalButton().click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

    }

    @Test(priority = 6, dependsOnMethods = "navigateToCoachingPortal", description = "Validate the notes in My Day portal")
    public void validateMyDayPortal() {

        MyDayPage myDayPage = new MyDayPage(driver);
        myDayPage.getSortByTime().click();
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        myDayPage.getSortByTime().click();
        System.out.println("Double Clicked on Sort by time button");
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        myDayPage.getNotesButtonForTime(startTime).click();
        System.out.println("Notes Button clicked");

        driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);

        WebElement notesTextBox = myDayPage.getNotesTextBox();
        notesTextBox.click();

        String textInNotesTextArea = notesTextBox.getAttribute("value");
        System.out.println("notes " + textInNotesTextArea);
        String textSetInEvents = eventDetails.getEventNotes().trim();
        System.out.println("events " + textSetInEvents);

        Assert.assertEquals(textSetInEvents, textInNotesTextArea, "The Notes in the events do not match with the text in My Day portal");
        myDayPage.getCloseButton();
    }

    @Test(priority = 7, dependsOnMethods = "navigateToCoachingPortal", description = "Failed Test Case")
    public void failedTest() {
        Assert.assertEquals(1, 2, "Test failed");
    }

    @Test(priority = 8, dependsOnMethods = "failedTest", description = "Skipped Test")
    public void skippedTestCase() {
        Assert.assertEquals(1, 1, "Skipped test");
    }

    @AfterSuite
    public void afterSuite() throws InterruptedException {
        Thread.sleep(3000);
        driver.quit();
    }
}
