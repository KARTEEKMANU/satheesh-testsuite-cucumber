package com.newtopia.stepDefinitions;

import com.newtopia.constants.DataConstants;
import com.newtopia.constants.UrlConstants;
import com.newtopia.data.BaseTest;
import com.newtopia.data.EventDetails;
import com.newtopia.data.SharedData;
import com.newtopia.pages.*;
import cucumber.api.java8.En;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import static com.newtopia.constants.UrlConstants.*;

public class MyDaySteps extends BaseTest implements En {

    private WebDriver driver;
    private SFHomePage sfHomePage;
    private EventDetails eventDetails;
    private String startTime;
    private static String notes = "Test notes " + Calendar.getInstance().getTime();

    public MyDaySteps(SharedData sharedData){
        super(sharedData);

        Given("I have to login to sf", () -> {
            driver = sharedData.getDriver();
            driver.get(SALESFORCE_URL);
            driver.manage().window().maximize();
            Assert.assertEquals(driver.getTitle(), "Login | Salesforce", "Login page title validation failed");
            LoginPage loginPage = new LoginPage(driver);
            loginPage.loginPageValidator(emailId, UrlConstants.password);

            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            //waiting for the page to load
            sfHomePage = new SFHomePage(driver);
            WebDriverWait webDriverWait = new WebDriverWait(driver, 120);
            webDriverWait.until(ExpectedConditions.visibilityOf(sfHomePage.getAllTabsButton()));
            Assert.assertEquals(driver.getTitle(), "Salesforce - Enterprise Edition", "Title after login validation failed");
        });

        When("I am on the home page of sf and click on events button", () -> {
            sfHomePage.getAllTabsButton().click();
            System.out.println("+ Button is clicked");

            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            //scroll to the coaching portal object
            JavascriptExecutor js = (JavascriptExecutor) driver;
            js.executeScript("arguments[0].scrollIntoView(true);", sfHomePage.getEventsButton());

            sfHomePage.getEventsButton().click();
            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        });

        Then("^i navigate to the events page and create new events with status (.*) type (.*) source (.*) method (.*)$",
                (String status, String type, String source, String method) -> {

                    EventsPage eventsPage = new EventsPage(driver);
                    eventsPage.getNewButton().click();

                    driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

                    CreateEventsPage createEventsPage = new CreateEventsPage(driver);
                    createEventsPage.getParticipantTextBx().click();
                    Thread.sleep(3000);
                    createEventsPage.getParticipantTextBx().sendKeys(PARTICIPANT);

                    Select select = new Select(createEventsPage.getStatusTab());
                    select.selectByVisibleText(status);

                    Select typeSelect = new Select(createEventsPage.getTypeTab());
                    typeSelect.selectByVisibleText(DataConstants.COACHING_SESSION);

                    Select sourceSelect = new Select(createEventsPage.getSourceTab());
                    sourceSelect.selectByVisibleText(DataConstants.INBOUND);

                    createEventsPage.getDatePicker().click();

                    createEventsPage.getNotesTextField().click();
                    createEventsPage.getNotesTextField().sendKeys(notes);

                    Select methodSelect = new Select(createEventsPage.getMethod());
                    methodSelect.selectByVisibleText(DataConstants.PHONE);

                    createEventsPage.getSaveBtn().click();
        });

        When("i navigate to the edit event details page and capture all the details", () -> {
            Thread.sleep(5000);
            EventEditDetailsPage eventEditDetailsPage = new EventEditDetailsPage(driver);
            System.out.println("the start time is " + eventEditDetailsPage.getStartTime());

            String startDate = eventEditDetailsPage.getStartTime();
            String time = startDate.substring(10);
            System.out.println("time " + time + " length is " + time.length());
            if(time.length() == 7) {
                startTime = "0" + startDate.substring(10); //appending '0' example '7:14 PM' becomes '07:14 PM'
            }
            else {
                startTime = startDate.substring(10);
            }
            System.out.println("time is [" + startTime + "]");

            eventDetails = new EventDetails();
            eventDetails.setEventStartTime(startTime);
            eventDetails.setEventNotes(notes);

            //TODO get all the event details
        });

        When("i navigate back to coaching portal", () -> {
            sfHomePage.getAllTabsButton().click();
            System.out.println("+ Button is clicked");

            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            //scroll to the coaching portal object
            JavascriptExecutor js = (JavascriptExecutor) driver;
            js.executeScript("arguments[0].scrollIntoView(true);", sfHomePage.getCoachingPortalButton());

            sfHomePage.getCoachingPortalButton().click();
            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        });

        Then("i validate my day portal", () -> {
            MyDayPage myDayPage = new MyDayPage(driver);
            myDayPage.getSortByTime().click();
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            myDayPage.getSortByTime().click();
            System.out.println("Double Clicked on Sort by time button");
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            myDayPage.getNotesButtonForTime(startTime).click();
            System.out.println("Notes Button clicked");

            driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);

            WebElement notesTextBox = myDayPage.getNotesTextBox();
            notesTextBox.click();

            String textInNotesTextArea = notesTextBox.getAttribute("value");
            System.out.println("notes " + textInNotesTextArea);
            String textSetInEvents = eventDetails.getEventNotes().trim();
            System.out.println("events " + textSetInEvents);

            Assert.assertEquals(textSetInEvents, textInNotesTextArea, "The Notes in the events do not match with the text in My Day portal");
            myDayPage.getCloseButton();
        });
    }
}
