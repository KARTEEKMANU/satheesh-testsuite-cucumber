package com.newtopia.stepDefinitions;

import com.newtopia.data.BaseTest;
import com.newtopia.data.SharedData;
import com.newtopia.runner.CucumberRunnerTest;
import cucumber.api.java8.En;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class DriverConfig extends BaseTest implements En {

    public DriverConfig(SharedData sharedData) {
        super(sharedData);
        Given("i instantiate the chrome driver", () -> {
            WebDriver driver = CucumberRunnerTest.driverInUse;
            sharedData.setDriver(driver);
        });
    }
}
