package com.newtopia.stepDefinitions;

import com.newtopia.constants.UrlConstants;
import com.newtopia.data.BaseTest;
import com.newtopia.data.SharedData;
import com.newtopia.pages.CalenderPage;
import com.newtopia.pages.LoginPage;
import com.newtopia.pages.MyDayPage;
import com.newtopia.pages.SFHomePage;
import com.newtopia.utils.URLPathGenerator;
import cucumber.api.java8.En;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.util.Date;
import java.util.concurrent.TimeUnit;

import static com.newtopia.constants.UrlConstants.SALESFORCE_URL;
import static com.newtopia.constants.UrlConstants.emailId;

public class TestMyDayFlow extends BaseTest implements En {
    private WebDriver driver;
    private SFHomePage sfHomePage;

    public TestMyDayFlow(SharedData sharedData) throws InterruptedException {
        super(sharedData);

        Given("I login to sales force as an inspirator", () -> {
            driver = sharedData.getDriver();
            driver.get(SALESFORCE_URL);
            driver.manage().window().maximize();
            Assert.assertEquals(driver.getTitle(), "Login | Salesforce", "Login page title validation failed");
            LoginPage loginPage = new LoginPage(driver);
            loginPage.loginPageValidator(emailId, UrlConstants.password);

            //waiting for the page to load
            sfHomePage= new SFHomePage(driver);
            WebDriverWait webDriverWait = new WebDriverWait(driver, 20);
            webDriverWait.until(ExpectedConditions.elementToBeClickable(sfHomePage.getAllTabsButton()));
            Assert.assertEquals(driver.getTitle(), "Salesforce - Enterprise Edition", "Title after login validation failed");
        });

        When("I click on the menu icon to see the list of objects", () -> {
            sfHomePage.getAllTabsButton().click();
            System.out.println("+ Button is clicked");
        });

        When("I search for coaching portal and click on it and navigate to My Day page", () -> {
            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            //scroll to the coaching portal object
            JavascriptExecutor js = (JavascriptExecutor) driver;
            js.executeScript("arguments[0].scrollIntoView(true);", sfHomePage.getCoachingPortalButton());

            sfHomePage.getCoachingPortalButton().click();
            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

            //Verifying that the My Day page is loaded
            String myDayPageUrl = driver.getCurrentUrl();
            String urlPath = URLPathGenerator.getPathForURL(myDayPageUrl);
            Assert.assertEquals(urlPath, "/apex/InspiratorMyDay", "\"My Day\" page is not loaded");
        });

        Then("I navigate to the my day screen and validate", () -> {

            MyDayPage myDayPage = new MyDayPage(driver);

            //check boxes test
            myDayPage.getScheduleOnlyChkBox().click();
            myDayPage.getExceptionsOnlyChkBox().click();

            //select a date
            WebElement selectADate = myDayPage.getDateField();
            selectADate.click();
            selectADate.clear();
            //TODO use Joda class to get the correct date format rather than hardcoding
            selectADate.sendKeys("Jul 17, 2020");
            myDayPage.getSubmitButton().click();

            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            myDayPage.validateTableNotesAndClickOpenButton();
        });
        

        Then("I open the dashboard calender", () -> {
        	MyDayPage myDayPage = new MyDayPage(driver);
        	myDayPage.getCalendarDeepLink().click();
       
        	String winHandleBefore = driver.getWindowHandle();

            for (String winHandle : driver.getWindowHandles()) {
                driver.switchTo().window(winHandle);
            }
            
            String calendarPageUrl = driver.getCurrentUrl();
            String calenderPagePath = URLPathGenerator.getPathForURL(calendarPageUrl);
            System.out.println("The calender page Path is " + calenderPagePath);
            Assert.assertEquals("/apex/Dashboard", calenderPagePath, "Calendar Page is not loaded");
            
            
            CalenderPage calendarPage = new CalenderPage(driver);
            
            WebDriverWait wait = new WebDriverWait(driver, 30);
            wait.until(ExpectedConditions.visibilityOf(calendarPage.getTodayTab()));
            
            calendarPage.clickOnTime();
            
            Thread.sleep(5000);
           
        	
        });

//        Then("I create pre dated event", () -> {
//        	System.out.println("to be implemented");
//        	
//
//        });
//
//        Then("I create a post dated event", () -> {
//        	System.out.println("to be implemented");
//        	
//
//        });
//
//        Then("I create a present date event", () -> {
//        	System.out.println("to be implemented");
//        	
//        	
//        });
    }
}
