package com.newtopia.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class EventsPage {

    protected static WebDriver driver;

    private final static By newButton = By.xpath("//input[@title='New']");

    public WebElement getNewButton() {
        return driver.findElement(newButton);
    }

    public EventsPage(WebDriver driver) {
        this.driver = driver;
    }
}
