package com.newtopia.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class EventEditDetailsPage {

    //td[text()='Start Time']/following-sibling::td

    protected static WebDriver driver;

    private final static By startTime = By.xpath("//td[text()='Start Time']/following-sibling::td");

    public String getStartTime() {
        return driver.findElement(startTime).getText();
    }

    public EventEditDetailsPage(WebDriver driver) {
        this.driver = driver;
    }
}
