package com.newtopia.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.json.JsonOutput;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public class MyDayPage {

    protected static WebDriver driver;

    private static final By showExceptionsOnlyChBox = By.xpath("//span[text()='Show Exceptions']");
    private static final By showScheduleOnlyChBox = By.xpath("//span[text()='Show Scheduled Only']");
    private static final By submitBtn = By.xpath("//button[@title='Click to load report data']");
    private static final By selectADate = By.xpath("//input[@name='date']");
    private static final By tableElements = By.xpath("//table[@role='grid']/tbody/tr");
    private static final By closeBtn = By.xpath("(//div[@class='slds-modal__footer']/button[@type='button'])[1]");
    private static final By notesTextBox = By.xpath("//textarea[@name='EventNotesTextArea']");
    private static final By calendarDeepLink = By.xpath("//a[text()='Open Dashboard Calendar']");
    private static final By sortByTime = By.xpath("//span[@title='Start Time']");

    private static final By selectedDate = By.xpath("//lightning-formatted-date-time");// TODO

    private static final By showBookingNotesBtn = By.xpath("//button[@name='ShowBookingNotes' and @title='Notes'])[1]");

    public WebElement getExceptionsOnlyChkBox() {
        return driver.findElement(showExceptionsOnlyChBox);
    }

    public WebElement getScheduleOnlyChkBox() {
        return driver.findElement(showScheduleOnlyChBox);
    }

    public WebElement getDateField() {
        return driver.findElement(selectADate);
    }

    public WebElement getSubmitButton() {
        return driver.findElement(submitBtn);
    }

    public int getTableElementsSize() {
        return driver.findElements(tableElements).size();
    }

    public WebElement getCloseButton() {
        return driver.findElement(closeBtn);
    }

    public WebElement getNotesTextBox() {
        return driver.findElement(notesTextBox);
    }

    public WebElement getCalendarDeepLink() {
        return driver.findElement(calendarDeepLink);
    }

    public WebElement getSortByTime() {
        return driver.findElement(sortByTime);
    }

    public WebElement getNotesButtonForTime(String selectedTime) {
        
        WebElement noteBtn = null;

        for (int i = 1; i <= 3; i++) {
            String text = driver.findElement(By.xpath("//lightning-formatted-date-time[" + i + "]")).getText();
            System.out.println("The text is [" + text + "]");
            if (text.equals(selectedTime)) {
                noteBtn = driver.findElement(By.xpath("(//button[@name='ShowBookingNotes' and @title='Notes'])["+ i + "]"));
                System.out.println("Notes is clicked " + i);
                break;
            }
        }
        return noteBtn;
    }

    public void validateTableNotesAndClickOpenButton() {
        driver.manage().window().setSize(new Dimension(2000, 2000));
        for (int i = 1; i <= getTableElementsSize(); i++) {

            WebElement notesBtn = driver.findElement(By.xpath("(//table[@role='grid']/tbody/tr)[" + i + "]/td[@data-label='Notes']"));
            notesBtn.click(); // to open the text pop up

            //close the pop up
            driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
            getNotesTextBox().clear();
            getNotesTextBox().sendKeys("I am notes" + i);

            driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
            getCloseButton().click();
            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            try {
                Thread.sleep(15000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            WebDriverWait webDriverWait = new WebDriverWait(driver, 30);

            //TODO check the invisibility of the green alert element
//            webDriverWait.until(ExpectedConditions.invisibilityOf
//                    (driver.findElement
//                            (By.xpath("//div[@text()='Event note has been updated']"))));
            getSubmitButton().click();
            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

            WebElement openBtn = driver.findElement(By.xpath("((//table[@role='grid']/tbody/tr)[" + i + "]/td)[10]"));
            openBtn.click();
            driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
            webDriverWait.until(ExpectedConditions.numberOfWindowsToBe(2));

            String winHandleBefore = driver.getWindowHandle();

            for (String winHandle : driver.getWindowHandles()) {
                driver.switchTo().window(winHandle);
            }
            driver.close();
            driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
            driver.switchTo().window(winHandleBefore);
            driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        }
    }


    public MyDayPage(WebDriver driver) {
        this.driver = driver;
    }
}
