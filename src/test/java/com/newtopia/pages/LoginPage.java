package com.newtopia.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LoginPage {

    protected WebDriver driver;

    private static final By loginId = By.xpath("//input[@id='username']");
    private static final By loginPassword = By.xpath("//input[@id='password']");
    private static final By logInBtn = By.xpath("//input[@id='Login']");

    public LoginPage(WebDriver driver) {
        this.driver = driver;
    }

    public WebElement getLoginId(){
        return driver.findElement(loginId);
    }

    public WebElement getLoginPassword(){
        return driver.findElement(loginPassword);
    }

    public WebElement getLoginButton(){
        return driver.findElement(logInBtn);
    }

    public LoginPage loginPageValidator(String userName, String password) {
        getLoginId().sendKeys(userName);
        getLoginPassword().sendKeys(password);
        getLoginButton().click();
        return new LoginPage(driver);
    }
}
