package com.newtopia.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class SFHomePage {

    protected static WebDriver driver;

    private final static By allTabsButton = By.xpath("//img[@title='All Tabs']");
    private final static By coachingPortalButton = By.xpath("//img[@title='Coaching Portal']");
    private final static By eventsButton = By.xpath("//img[@title='Events']");

    public WebElement getAllTabsButton() {
        return driver.findElement(allTabsButton);
    }

    public WebElement getCoachingPortalButton() {
        return driver.findElement(coachingPortalButton);
    }

    public WebElement getEventsButton() {
        return driver.findElement(eventsButton);
    }

    public SFHomePage(WebDriver driver) {
        this.driver = driver;
    }
}
