package com.newtopia.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class CalenderPage {
	
    protected WebDriver driver;

    private static final By todayTab = By.xpath("//button[text()='today']"); //== Done
    private static final By today = 
    		By.xpath("//th[@data-date='2020-08-03']/span[text()='Mon 8/3']"); // 11:20
    private static final By loginPassword = By.xpath("//input[@id='password']");
    private static final By logInBtn = By.xpath("//input[@id='Login']");

    public CalenderPage(WebDriver driver) {
        this.driver = driver;
    }

    public WebElement getTodayTab(){
        return driver.findElement(todayTab);
    }
    
    public WebElement getToday() {
    	return driver.findElement(today);
    }

    public void clickOnTime() {
    	
    	int x = getToday().getLocation().getX();
    	int y = getToday().getLocation().getY();
    	System.out.println("The x is +200" + x + " y is +5 " + y);
    	
    	Actions builder = new Actions(driver);
    	builder.moveToElement(getToday(), x + 10, y - 10).click().build().perform();
    }

}
