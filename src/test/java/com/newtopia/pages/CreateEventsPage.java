package com.newtopia.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class CreateEventsPage {

    protected static WebDriver driver;

    private final static By participantTextBx = By.xpath("(//span[@class='lookupInput'])[2]/input");
    private final static By statusTab = By.xpath("//select[@tabindex='5']");
    private final static By typeTab = By.xpath("//select[@tabindex='6']");
    private final static By sourceTab = By.xpath("//select[@tabindex='9']");
    private final static By datePicker = By.xpath("(//span[@class='dateFormat'])[2]/a");
    private final static By method = By.xpath("//select[@tabindex='17']");
    private final static By saveBtn = By.xpath("//input[@tabindex='19' and @title='Save']");
    private final static By notesTextField = By.xpath("//tr[4]/td/textarea");

    public WebElement getParticipantTextBx() {
        return driver.findElement(participantTextBx);
    }

    public WebElement getStatusTab() {
        return driver.findElement(statusTab);
    }

    public WebElement getTypeTab() {
        return driver.findElement(typeTab);
    }

    public WebElement getSourceTab() {
        return driver.findElement(sourceTab);
    }

    public WebElement getDatePicker() {
        return driver.findElement(datePicker);
    }

    public WebElement getMethod() {
        return driver.findElement(method);
    }

    public WebElement getSaveBtn() {
        return driver.findElement(saveBtn);
    }

    public WebElement getNotesTextField(){
        return driver.findElement(notesTextField);
    }


    public CreateEventsPage(WebDriver driver) {
        this.driver = driver;
    }
}
