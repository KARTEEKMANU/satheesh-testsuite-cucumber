package com.newtopia.data;


import org.openqa.selenium.WebDriver;

import java.util.HashMap;
import java.util.Map;

public class SharedData {

    private WebDriver driver;
    private final Map<String, Object> sharedValue = new HashMap<>();

    public WebDriver getDriver() {
        return driver;
    }

    public void setDriver(WebDriver driver) {
        this.driver = driver;
    }

    public Map<String, Object> getSharedValue() {
        return sharedValue;
    }
}
