package com.newtopia.utils;

import java.net.MalformedURLException;
import java.net.URL;

public class URLPathGenerator {

    public static String getPathForURL(String requiredUrl) {
        URL url = null;
        try {
            url = new URL(requiredUrl);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        String path = url.getPath();
        return path != null ? path : "no path";
    }
    
    public static String getQueryParamsForURL(String requiredUrl) {
    	URL url = null;
    	try {
			url = new URL(requiredUrl);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	String queryParams = url.getQuery();
    	return queryParams != null ? queryParams : "no query params";
    }
}
