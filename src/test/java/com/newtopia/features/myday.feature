Feature: Test the My Day page

    Background: instantiate the chrome driver

        Given i instantiate the chrome driver

    Scenario Outline: test the My Day page
        Given I have to login to sf
        When I am on the home page of sf and click on events button
        And i navigate to the events page and create new events with status <status> type <type> source <source> method <method>
        And i navigate to the edit event details page and capture all the details
        And i navigate back to coaching portal
        Then i validate my day portal

        Examples:
            | status    | type             | source  | method |
            | scheduled | coaching_session | Inbound | phone  |


