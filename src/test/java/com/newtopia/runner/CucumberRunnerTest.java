package com.newtopia.runner;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;


@CucumberOptions(glue = {"com.newtopia.stepDefinitions"},
        features = {"./src/test/java/com/newtopia/features/"},
        plugin = {"pretty", "json:build/cucumber-report.json", "html:build/cucumber-html-report"}
        )
public class CucumberRunnerTest extends AbstractTestNGCucumberTests {

    public static WebDriver driverInUse;

    @BeforeSuite
    public void beforeSuite() {
        String osName = System.getProperty("os.name");
        if (osName.contains("MAC") || osName.contains("Mac")) {
            System.setProperty("webdriver.chrome.driver", "src/test/java/com/newtopia/divers/chromedriver_mac");
            driverInUse = new ChromeDriver();
        } else {
            System.out.println("The os is not determined");
        }
    }

    @AfterSuite
    public void afterSuite() {
        driverInUse.quit();
    }
}
